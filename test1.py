dict_t = {
    "data": {
        "entityUrn": "urn:li:collectionResponse:snqxxL4KYaESt/uH8lkFVVwXvR4y+ckZ07Bshs1kgCM=",
        "elements": [
    
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853346754654437376,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853651204568829952,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853619355792482304,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853347439089332224,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853651421502418944,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6854504945928953857,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853620131906506752,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853616639829655552,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6853624041253724160,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            },
            {
                "*updateV2": "urn:li:fs_updateV2:(urn:li:activity:6763409213239656448,FEED_DETAIL,EMPTY,DEFAULT,true)",
                "showInterstitial": False,
                "rejected": False,
                "$type": "com.linkedin.voyager.organization.feed.OrganizationAdsTransparencyUpdate"
            }
        ],
        "paging": {
            "count": 10,
            "start": 13,
            "total": 353,
            "links": []
        },
        "$type": "com.linkedin.restli.common.CollectionResponse"
    },
    "included": [
        {
            "thumbnail": {
                "artifacts": [
                    {
                        "width": 720,
                        "fileIdentifyingUrlPathSegment": "high/0/1634031297891?e=1634842800&v=beta&t=b9Diq9RAJdwkVfu78u_PTwcZcp9gNmQivVNK5PvUPzk",
                        "expiresAt": 1634842800000,
                        "height": 720,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 360,
                        "fileIdentifyingUrlPathSegment": "low/0/1634031297920?e=1634842800&v=beta&t=LxQ-ySNErLHnA88Dqx4WFR9kjk5NYI3x17F1_tlZgXo",
                        "expiresAt": 1634842800000,
                        "height": 360,
                        "$type": "com.linkedin.common.VectorArtifact"
                    }
                ],
                "rootUrl": "https://media-exp1.licdn.com/dms/image/C4E05AQHlgCRwb-afBw/videocover-",
                "$type": "com.linkedin.common.VectorImage"
            },
            "progressiveStreams": [
                {
                    "streamingLocations": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4E05AQHlgCRwb-afBw/mp4-720p-30fp-crf28/0/1634031304554?e=1634842800&v=beta&t=eh8FDRoJHfP_HTcpqfQBSrBCEQ3Y47GTS5KBkl91vjw",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "size": 2039665,
                    "bitRate": 163293,
                    "width": 720,
                    "mediaType": "video/mp4",
                    "height": 720,
                    "$type": "com.linkedin.videocontent.ProgressiveDownloadMetadata"
                },
                {
                    "streamingLocations": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4E05AQHlgCRwb-afBw/mp4-640p-30fp-crf28/0/1634031304234?e=1634842800&v=beta&t=XqEgfyLcRJHcIlOrUFd45RcK2hfUd5zd9iKBJ8XGaTI",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "size": 1948700,
                    "bitRate": 146302,
                    "width": 640,
                    "mediaType": "video/mp4",
                    "height": 640,
                    "$type": "com.linkedin.videocontent.ProgressiveDownloadMetadata"
                }
            ],
            "liveStreamCreatedAt": None,
            "transcripts": [
                {
                    "captionFormat": "WEBVTT",
                    "locale": {
                        "language": "en",
                        "$type": "com.linkedin.common.Locale"
                    },
                    "captionFile": "https://dms.licdn.com/playlist/C4E05AQHlgCRwb-afBw/feedshare-video-captions-thumbnails-ambry-webvtt/0/1634031286626/output_captions_ad1ee32c-430f-4373-9902-f465526576f6.vtt?e=1634842800&v=beta&t=FtCyB46Iuhuy0byHg6d1th83IFrKCl_lhV7VSKVBNBE",
                    "isAutogenerated": False,
                    "$type": "com.linkedin.videocontent.Transcript"
                },
                {
                    "captionFormat": "SRT",
                    "locale": {
                        "language": "en",
                        "$type": "com.linkedin.common.Locale"
                    },
                    "captionFile": "https://dms.licdn.com/playlist/C4E05AQHlgCRwb-afBw/feedshare-video-captions-thumbnails-ambry-srt/0/1634031286653/output_captions_311ebaa7-3ae3-489f-82b1-20c584eac270.srt?e=1634842800&v=beta&t=UaVXzT7CQUE6Th7v75XPez5RDsyDOwLlDp2CTplRapA",
                    "isAutogenerated": False,
                    "$type": "com.linkedin.videocontent.Transcript"
                }
            ],
            "prevMedia": None,
            "aspectRatio": 1,
            "media": "urn:li:digitalmediaAsset:C4E05AQHlgCRwb-afBw",
            "adaptiveStreams": [
                {
                    "initialBitRate": 304184,
                    "protocol": "HLS",
                    "mediaType": "application/vnd.apple.mpegURL",
                    "masterPlaylists": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4E05AQHlgCRwb-afBw/hls-360p-720p-crf28/0/1634031304525?e=1634842800&v=beta&t=451d8H2aZYi2P1_4H8VYtTodXS-pkkqWyYBqCSoIbcQ",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "$type": "com.linkedin.videocontent.AdaptiveStream"
                }
            ],
            "$type": "com.linkedin.videocontent.VideoPlayMetadata",
            "liveStreamEndedAt": None,
            "duration": 55033,
            "entityUrn": "urn:li:digitalmediaAsset:C4E05AQHlgCRwb-afBw",
            "provider": "ADS",
            "nextMedia": None,
            "thumbnails": None,
            "trackingId": "oypp8S9nSRGiFk/BIuuqqA=="
        },
        {
            "thumbnail": {
                "artifacts": [
                    {
                        "width": 624,
                        "fileIdentifyingUrlPathSegment": "_350_624/0/1612522468968/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=z6NSpzDtk_YXYGxC-BEwJMlhfvwXvqiKyIGqmrsoc8w",
                        "expiresAt": 1634842800000,
                        "height": 350,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 1280,
                        "fileIdentifyingUrlPathSegment": "-high/0/1612522458612/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=JsjiwTpFM9w75U5xO1yPmNeYZgzPGsPuHsbOSBIBR-o",
                        "expiresAt": 1634842800000,
                        "height": 720,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 640,
                        "fileIdentifyingUrlPathSegment": "-low/0/1612522458613/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=v2rgxOuRJ4Y_x94Iz28k8wjALD2L1kAQzdrDjtmmNNE",
                        "expiresAt": 1634842800000,
                        "height": 360,
                        "$type": "com.linkedin.common.VectorArtifact"
                    }
                ],
                "rootUrl": "https://media-exp1.licdn.com/dms/image/C4D10AQGhKzwFNV-_2g/videocover",
                "$type": "com.linkedin.common.VectorImage"
            },
            "progressiveStreams": [
                {
                    "streamingLocations": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4D10AQGhKzwFNV-_2g/mp4-720p-30fp-crf28/0/1612522467001/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=dboM8yEq9UqG33GoepYx-tgIY4qd1UqCKVYDxAlP1iY",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "size": 1040421,
                    "bitRate": 362087,
                    "width": 1280,
                    "mediaType": "video/mp4",
                    "height": 720,
                    "$type": "com.linkedin.videocontent.ProgressiveDownloadMetadata"
                },
                {
                    "streamingLocations": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4D10AQGhKzwFNV-_2g/mp4-640p-30fp-crf28/0/1612522463749/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=RUjV9YILE6mLPrlXFuSgMKqaRbdxnAHUoRsBFovTTw4",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "size": 927865,
                    "bitRate": 307173,
                    "width": 1138,
                    "mediaType": "video/mp4",
                    "height": 640,
                    "$type": "com.linkedin.videocontent.ProgressiveDownloadMetadata"
                },
                {
                    "streamingLocations": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4D10AQGhKzwFNV-_2g/mp4-360p-30fp-crf28/0/1612522463017/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=oHjRKaiUHNtrRH3OvOdYm0ZZ0qNyUE_Sp9_Cis78GaA",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "size": 497741,
                    "bitRate": 132981,
                    "width": 640,
                    "mediaType": "video/mp4",
                    "height": 360,
                    "$type": "com.linkedin.videocontent.ProgressiveDownloadMetadata"
                }
            ],
            "liveStreamCreatedAt": None,
            "transcripts": [],
            "prevMedia": None,
            "aspectRatio": 1.7777778,
            "media": "urn:li:digitalmediaAsset:C4D10AQGhKzwFNV-_2g",
            "adaptiveStreams": [
                {
                    "initialBitRate": 600472,
                    "protocol": "HLS",
                    "mediaType": "application/vnd.apple.mpegURL",
                    "masterPlaylists": [
                        {
                            "url": "https://dms.licdn.com/playlist/C4D10AQGhKzwFNV-_2g/hls-360p-720p-crf28/0/1612522466707/HUBS-CRMPlatformLookLike-15-UK-YT-001mp4?e=1634842800&v=beta&t=FhDZgEQpGzl6G1ovYWuDXLzqo8AsD0pr9g5OFaQqYs4",
                            "expiresAt": 1634842800000,
                            "$type": "com.linkedin.videocontent.StreamingLocation"
                        }
                    ],
                    "$type": "com.linkedin.videocontent.AdaptiveStream"
                }
            ],
            "$type": "com.linkedin.videocontent.VideoPlayMetadata",
            "liveStreamEndedAt": None,
            "duration": 16666,
            "entityUrn": "urn:li:digitalmediaAsset:C4D10AQGhKzwFNV-_2g",
            "provider": "ADS",
            "nextMedia": None,
            "thumbnails": None,
            "trackingId": "dGmaRWSlSluOXJ4xFjiVTg=="
        },
        {
            "entityUrn": "urn:li:fs_followingInfo:urn:li:company:68529",
            "following": False,
            "dashFollowingStateUrn": "urn:li:fsd_followingState:urn:li:fsd_company:68529",
            "trackingUrn": "urn:li:company:68529",
            "followingType": "DEFAULT",
            "followerCount": 640226,
            "followingCount": None,
            "$type": "com.linkedin.voyager.common.FollowingInfo"
        },
        {
            "objectUrn": "urn:li:company:68529",
            "entityUrn": "urn:li:fs_miniCompany:68529",
            "name": "HubSpot",
            "showcase": False,
            "active": True,
            "logo": {
                "artifacts": [
                    {
                        "width": 200,
                        "fileIdentifyingUrlPathSegment": "200_200/0/1519875026111?e=1642636800&v=beta&t=huizWS_K4DDt80aiJX7YoDfCVsjqTorUVVHRAfpxB9w",
                        "expiresAt": 1642636800000,
                        "height": 200,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 100,
                        "fileIdentifyingUrlPathSegment": "100_100/0/1519875026111?e=1642636800&v=beta&t=bfEweeFnfG-qL_MzF7_dlOBVSvHrFDpfl6sxHnebt1I",
                        "expiresAt": 1642636800000,
                        "height": 100,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 400,
                        "fileIdentifyingUrlPathSegment": "400_400/0/1519875026111?e=1642636800&v=beta&t=UqjfBAePgthgzhWv4d27SdyhFSAvRikyXq_HJP0D6rM",
                        "expiresAt": 1642636800000,
                        "height": 400,
                        "$type": "com.linkedin.common.VectorArtifact"
                    }
                ],
                "rootUrl": "https://media-exp1.licdn.com/dms/image/C560BAQFhSyJmEbHWJw/company-logo_",
                "$type": "com.linkedin.common.VectorImage"
            },
            "universalName": "hubspot",
            "dashCompanyUrn": "urn:li:fsd_company:68529",
            "trackingId": "goNWKRJISXWuQ8yYpawDJQ==",
            "$type": "com.linkedin.voyager.entities.shared.MiniCompany"
        },
        {
            "dashEntityUrn": None,
            "entityUrn": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
            "saved": False,
            "$type": "com.linkedin.voyager.feed.render.SaveAction"
        },
        {
            "dashEntityUrn": None,
            "entityUrn": "urn:li:fs_saveAction:(SAVE,urn:li:article:8244811414927270826)",
            "saved": False,
            "$type": "com.linkedin.voyager.feed.render.SaveAction"
        },
        {
            "dashEntityUrn": None,
            "entityUrn": "urn:li:fs_saveAction:(SAVE,urn:li:article:7982154608927201533)",
            "saved": False,
            "$type": "com.linkedin.voyager.feed.render.SaveAction"
        },
        {
            "dashEntityUrn": None,
            "entityUrn": "urn:li:fs_saveAction:(SAVE,urn:li:article:7502468851983448090)",
            "saved": False,
            "$type": "com.linkedin.voyager.feed.render.SaveAction"
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "descriptionContainerNavigationContext": {
                    "actionTarget": "https://www.hubspot.com/crm-platform-hspd-brand?utm_medium=paid&utm_source=linkedin&utm_campaign=brandstrategy_views_en_nam_nam_prospecting_crmplatform_videos_ad2-15s&hsa_acc=500001079&hsa_cam=610596113&hsa_grp=175817793&hsa_ad=127290283&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "Grow Better With HubSpot . View sponsored content",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/crm-platform-hspd-brand?utm_medium=paid&utm_source=linkedin&utm_campaign=brandstrategy_views_en_nam_nam_prospecting_crmplatform_videos_ad2-15s&hsa_acc=500001079&hsa_cam=610596113&hsa_grp=175817793&hsa_ad=127290283&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "LEARN_MORE",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "mediaDisplayVariant": "CLASSIC",
                "inlineCtaButton": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/crm-platform-hspd-brand?utm_medium=paid&utm_source=linkedin&utm_campaign=brandstrategy_views_en_nam_nam_prospecting_crmplatform_videos_ad2-15s&hsa_acc=500001079&hsa_cam=610596113&hsa_grp=175817793&hsa_ad=127290283&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Grow Better With HubSpot ",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*videoPlayMetadata": "urn:li:digitalmediaAsset:C4D10AQGhKzwFNV-_2g",
                "$type": "com.linkedin.voyager.feed.render.LinkedInVideoComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6763409213239656448",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:ugcPost:6763409203051687936",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6763409213239656448,urn:li:sponsoredCreative:127290283)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:127290283",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE_VIDEO",
                        "authorUrn": "urn:li:sponsoredAccount:500001079",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:500001079",
                        "adUrn": "urn:li:sponsoredCreative:127290283",
                        "videoBehavior": "OPEN_VIDEO_FULLSCREEN",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/crm-platform-hspd-brand?utm_medium=paid&utm_source=linkedin&utm_campaign=brandstrategy_views_en_nam_nam_prospecting_crmplatform_videos_ad2-15s&hsa_acc=500001079&hsa_cam=610596113&hsa_grp=175817793&hsa_ad=127290283&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:127290283,1634756365871)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "dGmaRWSlSluOXJ4xFjiVTg==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6763409213239656448,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/crm-platform-hspd-brand?utm_medium=paid&utm_source=linkedin&utm_campaign=brandstrategy_views_en_nam_nam_prospecting_crmplatform_videos_ad2-15s&hsa_acc=500001079&hsa_cam=610596113&hsa_grp=175817793&hsa_ad=127290283&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored content",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot’s CRM platform is built for scaling companies.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7748953088820494932",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 200,
                                        "fileIdentifyingUrlPathSegment": "200_200/0/1634029528138?e=1637798400&v=beta&t=_gKi06Z1w8RtEleowgT65RdnOqv1Z_xMkitG9FC7QIY",
                                        "expiresAt": 1637798400000,
                                        "height": 150,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 836,
                                        "fileIdentifyingUrlPathSegment": "627_1200/0/1634029528138?e=1637798400&v=beta&t=aEb7OLUtSxdYnnyc_SCeq7HO0kUULDXiAbRoMZv-fcs",
                                        "expiresAt": 1637798400000,
                                        "height": 627,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1280,
                                        "fileIdentifyingUrlPathSegment": "1280/0/1634029528138?e=1637798400&v=beta&t=akPQWHwH7EeX9DzOVTCqXWcJD44xi5Iir-lHPqHVGpc",
                                        "expiresAt": 1637798400000,
                                        "height": 960,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQEl3iVEXFDjOQ/companyUpdate-article-image-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.75,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "hubspot.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_DACH_Q3_2021_Awareness&utm_content=Office_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191015923&hsa_ad=162377223&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_DACH_Q3_2021_Awareness&utm_content=Office_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191015923&hsa_ad=162377223&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Remote and office-based Sales and Customer Support positions!",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853616639829655552",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853616639464742912",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853616639829655552,urn:li:sponsoredCreative:162377223)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162377223",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162377223",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_DACH_Q3_2021_Awareness&utm_content=Office_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191015923&hsa_ad=162377223&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162377223,1634756365924)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "Yspi7tdHSoyKCSBgEkaiYw==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853616639829655552,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_DACH_Q3_2021_Awareness&utm_content=Office_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191015923&hsa_ad=162377223&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot is hiring in Germany! Find out more about our award-winning culture in our careers site and apply to one of our open positions.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7502468851983448090",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 800,
                                        "fileIdentifyingUrlPathSegment": "800/0/1634047955068?e=1634842800&v=beta&t=DM0waVl-F4AsdJPFHPSEML4IzJ1oCy07j43JRUNOIoI",
                                        "expiresAt": 1634842800000,
                                        "height": 400,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1000,
                                        "fileIdentifyingUrlPathSegment": "1280_800/0/1634047955068?e=1634842800&v=beta&t=rg4PDsZKKybUaGrNFq7q6dGMCfuQtzndG3eRh1lfbsU",
                                        "expiresAt": 1634842800000,
                                        "height": 500,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 160,
                                        "fileIdentifyingUrlPathSegment": "160/0/1634047955068?e=1634842800&v=beta&t=6G9E8D403vixhWxIrypj3te5QHteBxdE9R-sxip-Ak8",
                                        "expiresAt": 1634842800000,
                                        "height": 80,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 480,
                                        "fileIdentifyingUrlPathSegment": "480/0/1634047955068?e=1634842800&v=beta&t=FxRbgCdrm7qvh7EK3KoutXAzwnM4wYLsBtVX7Yy1naw",
                                        "expiresAt": 1634842800000,
                                        "height": 240,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/C4D27AQE-BDZvYCNLaQ/articleshare-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.5225,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "eventbrite.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7502468851983448090)",
                "navigationContext": {
                    "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222973&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222973&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Save the Date - October 19th - Growing Your Career in Tech Sales",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853347439089332224",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853347438799941633",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853347439089332224,urn:li:sponsoredCreative:162222973)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162222973",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162222973",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222973&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162222973,1634756365919)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "E7WIGIx+SqOKtOV/2Ypv2w==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853347439089332224,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222973&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Growing Your Career in Tech Sales - Join HubSpot on October 19th for a discussion around advancing your career in sales, finding balance, and breaking into tech sales for the first time. Sign-up for free today. ",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": None,
            "updateMetadata": {
                "urn": "urn:li:activity:6854504945928953857",
                "actionsPosition": "HEADER_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6854504945396273152",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6854504945928953857,urn:li:sponsoredCreative:163052703)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:163052703",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:500001079",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:500001079",
                        "adUrn": "urn:li:sponsoredCreative:163052703",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://offers.hubspot.com/social-media-content-calendar?utm_medium=paid&utm_source=linkedin&utm_campaign=Marketing_Leads_EN_NAM_NAM_Retargeting_SocialMediaContentCalendar-LeadAdForm_em453_Ad1&hsa_acc=500001079&hsa_cam=500001079&hsa_grp=191288383&hsa_ad=163052703&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:163052703,1634756365895)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "RDzgm1ogS16Le4POBZif2A==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6854504945928953857,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": {
                "ctaButton": {
                    "navigationContext": {
                        "actionTarget": "https://www.linkedin.com/leadGenForm/urn:li:fs_leadGenForm:(5528143,-1,-1)?leadTrackingUrl=",
                        "trackingActionType": "viewForm",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "commentary": {
                    "templateType": "DEFAULT",
                    "navigationContext": {
                        "actionTarget": "https://www.linkedin.com/leadGenForm/urn:li:fs_leadGenForm:(5528143,-1,-1)?leadTrackingUrl=",
                        "trackingActionType": "viewForm",
                        "accessibilityText": "View Form",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "numLines": 2,
                    "text": {
                        "textDirection": "FIRST_STRONG",
                        "text": "Plan and bulk upload your social media posts with this free template.",
                        "$type": "com.linkedin.voyager.common.TextViewModel"
                    },
                    "$type": "com.linkedin.voyager.feed.render.TextComponent"
                },
                "content": {
                    "templateType": "DEFAULT",
                    "urn": "urn:li:article:8244811414927270826",
                    "largeImage": {
                        "accessibilityTextAttributes": [],
                        "attributes": [
                            {
                                "useCropping": False,
                                "sourceType": "VECTOR",
                                "vectorImage": {
                                    "artifacts": [
                                        {
                                            "width": 200,
                                            "fileIdentifyingUrlPathSegment": "shrink_200_200/0/1634241825558?e=1637798400&v=beta&t=bSZl750Fu90B01G37wodoHSN-qu2u5huZvH5aLc5Ztg",
                                            "expiresAt": 1637798400000,
                                            "height": 104,
                                            "$type": "com.linkedin.common.VectorArtifact"
                                        },
                                        {
                                            "width": 1200,
                                            "fileIdentifyingUrlPathSegment": "shrink_627_1200/0/1634241825558?e=1637798400&v=beta&t=UtHLNzTwhP9JC3manM43tF-VH0bu32WVWgNKDslN4YI",
                                            "expiresAt": 1637798400000,
                                            "height": 627,
                                            "$type": "com.linkedin.common.VectorArtifact"
                                        },
                                        {
                                            "width": 1200,
                                            "fileIdentifyingUrlPathSegment": "shrink_1280/0/1634241825558?e=1637798400&v=beta&t=SkcRVJr4tHS3ii39oNJw_X8SMVtHuhVI5ePrHk7X604",
                                            "expiresAt": 1637798400000,
                                            "height": 627,
                                            "$type": "com.linkedin.common.VectorArtifact"
                                        },
                                        {
                                            "width": 1200,
                                            "fileIdentifyingUrlPathSegment": "offsite-ads_627_1200/0/1634752531531?e=1637798400&v=beta&t=MmMZKVYcwbE6zWlZBif-JhnYpWeRi9csYqm8FCGNYwM",
                                            "expiresAt": 1637798400000,
                                            "height": 627,
                                            "$type": "com.linkedin.common.VectorArtifact"
                                        }
                                    ],
                                    "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQHrEE1Q5VnbGg/companyUpdate-article-image-",
                                    "$type": "com.linkedin.common.VectorImage"
                                },
                                "displayAspectRatio": 0.5225,
                                "$type": "com.linkedin.voyager.common.ImageAttribute"
                            }
                        ],
                        "$type": "com.linkedin.voyager.common.ImageViewModel"
                    },
                    "subtitle": {
                        "textDirection": "USER_LOCALE",
                        "text": "offers.hubspot.com",
                        "$type": "com.linkedin.voyager.common.TextViewModel"
                    },
                    "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:8244811414927270826)",
                    "navigationContext": {
                        "actionTarget": "https://www.linkedin.com/leadGenForm/urn:li:fs_leadGenForm:(5528143,-1,-1)?leadTrackingUrl=",
                        "trackingActionType": "viewForm",
                        "accessibilityText": "View Form",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "type": "EXTERNAL_FULL",
                    "title": {
                        "textDirection": "FIRST_STRONG",
                        "text": "Social Media Content Calendar",
                        "$type": "com.linkedin.voyager.common.TextViewModel"
                    },
                    "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
                },
                "$type": "com.linkedin.voyager.feed.render.LeadGenFormContent",
                "*leadGenForm": "urn:li:fs_leadGenForm:(5528143,-1,-1)"
            },
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": None,
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7748953088820494932",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 200,
                                        "fileIdentifyingUrlPathSegment": "200_200/0/1634037776419?e=1637798400&v=beta&t=8zp1kCqHiiFzxuRgUm-oNdtNO16kRi63UL4zljn6Yu8",
                                        "expiresAt": 1637798400000,
                                        "height": 123,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 900,
                                        "fileIdentifyingUrlPathSegment": "627_1200/0/1634037776419?e=1637798400&v=beta&t=OtedR3YozZdY-2f-k8Kb79QIDk9Zcnh2isYpKPLgLSQ",
                                        "expiresAt": 1637798400000,
                                        "height": 555,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 900,
                                        "fileIdentifyingUrlPathSegment": "1280/0/1634037776419?e=1637798400&v=beta&t=DSKW3ImU5J2jUUuvvaHUo4hhB9S2_ccq5i6sMFMO4_Q",
                                        "expiresAt": 1637798400000,
                                        "height": 555,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQFfGWbOyvjDJw/companyUpdate-article-image-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.615,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "hubspot.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Group_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402623&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Group_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402623&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Remote and office-based Sales positions!",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853651204568829952",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853651204287811584",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853651204568829952,urn:li:sponsoredCreative:162402623)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162402623",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162402623",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Group_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402623&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162402623,1634756365880)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "Eob26bTCTn+Kgo+sds87Ow==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853651204568829952,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Group_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402623&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot is hiring in France! Find out more about our award-winning culture in our careers site and apply to one of our open positions.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7748953088820494932",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 200,
                                        "fileIdentifyingUrlPathSegment": "200_200/0/1634030362256?e=1637798400&v=beta&t=AO9C3tndWh69MAX0E_3cS45tCR6P2200HkiNtj4H1MU",
                                        "expiresAt": 1637798400000,
                                        "height": 148,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 842,
                                        "fileIdentifyingUrlPathSegment": "627_1200/0/1634030362256?e=1637798400&v=beta&t=YFgr0gfndMdZs6NIJ9M2iq5mXuzpDD-CkNagIh4bArc",
                                        "expiresAt": 1637798400000,
                                        "height": 627,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1054,
                                        "fileIdentifyingUrlPathSegment": "1280/0/1634030362256?e=1637798400&v=beta&t=psb6qSw1vhatyR4QiEXkUzTYtl6IDjuafasANXOQPFE",
                                        "expiresAt": 1637798400000,
                                        "height": 784,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQHQy-QUnyrP-w/companyUpdate-article-image-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.74,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "hubspot.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162380023&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162380023&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Remote and office-based Sales positions!",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853620131906506752",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853620131575160832",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853620131906506752,urn:li:sponsoredCreative:162380023)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162380023",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162380023",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162380023&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162380023,1634756365888)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "Qgz7+oDURZqgNaCpEn6ItQ==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853620131906506752,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_2&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162380023&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot is hiring in France! Find out more about our award-winning culture in our careers site and apply to one of our open positions.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7748953088820494932",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 200,
                                        "fileIdentifyingUrlPathSegment": "200_200/0/1634037829672?e=1637798400&v=beta&t=vApcROGbS6zMr5HcFxr8KoZ6kZ8UWsfvgYxg2oHqhI0",
                                        "expiresAt": 1637798400000,
                                        "height": 133,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 940,
                                        "fileIdentifyingUrlPathSegment": "627_1200/0/1634037829672?e=1637798400&v=beta&t=GmoUCZBpLPoA6l6WfKMV9dOjxidmzUUFBXAcH2CHfbY",
                                        "expiresAt": 1637798400000,
                                        "height": 627,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1280,
                                        "fileIdentifyingUrlPathSegment": "1280/0/1634037829672?e=1637798400&v=beta&t=6ZJBFbiJtvW3UqVi4K6Ow6yFkU5OXhVPLNumEG8c4tk",
                                        "expiresAt": 1637798400000,
                                        "height": 853,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQHglsXDZuSvAQ/companyUpdate-article-image-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.665,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "hubspot.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Formal_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402713&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Formal_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402713&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Remote and office-based Sales positions!",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853651421502418944",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853651421183664129",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853651421502418944,urn:li:sponsoredCreative:162402713)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162402713",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162402713",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Formal_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402713&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162402713,1634756365929)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "LrMCVnHqQ9yOVCoWEga3Gg==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853651421502418944,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Formal_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162402713&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot is hiring in France! Find out more about our award-winning culture in our careers site and apply to one of our open positions.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7982154608927201533",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 800,
                                        "fileIdentifyingUrlPathSegment": "800/0/1634047955068?e=1634842800&v=beta&t=DM0waVl-F4AsdJPFHPSEML4IzJ1oCy07j43JRUNOIoI",
                                        "expiresAt": 1634842800000,
                                        "height": 400,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1000,
                                        "fileIdentifyingUrlPathSegment": "1280_800/0/1634047955068?e=1634842800&v=beta&t=rg4PDsZKKybUaGrNFq7q6dGMCfuQtzndG3eRh1lfbsU",
                                        "expiresAt": 1634842800000,
                                        "height": 500,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 160,
                                        "fileIdentifyingUrlPathSegment": "160/0/1634047955068?e=1634842800&v=beta&t=6G9E8D403vixhWxIrypj3te5QHteBxdE9R-sxip-Ak8",
                                        "expiresAt": 1634842800000,
                                        "height": 80,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 480,
                                        "fileIdentifyingUrlPathSegment": "480/0/1634047955068?e=1634842800&v=beta&t=FxRbgCdrm7qvh7EK3KoutXAzwnM4wYLsBtVX7Yy1naw",
                                        "expiresAt": 1634842800000,
                                        "height": 240,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/C4D27AQE-BDZvYCNLaQ/articleshare-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.5225,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "eventbrite.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7982154608927201533)",
                "navigationContext": {
                    "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222363&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222363&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Register. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Register",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot Women in Sales Presents: Growing Your Career in Tech Sales",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853346754654437376",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853346754188853248",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853346754654437376,urn:li:sponsoredCreative:162222363)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162222363",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162222363",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222363&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162222363,1634756365913)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "ItNcIAeKQnyVQ4LoN0Ywcg==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853346754654437376,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.eventbrite.com/e/hubspot-women-in-sales-presents-growing-your-career-in-tech-sales-tickets-170298604711?aff=LinkedInAds&utm_campaign=WIS_Event_Q4_21&utm_source=linkedin&utm_medium=paid&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=190964703&hsa_ad=162222363&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Do you work in sales and want to make the move into tech sales? Want to learn how to make P-Club while enjoying a healthy work/life balance? Join HubSpot on October 19th at 11 am when we’ll sit down for a fireside chat discussion about excelling and growing a career in tech sales.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "templateType": "DEFAULT",
                "urn": "urn:li:article:7748953088820494932",
                "largeImage": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "useCropping": False,
                            "sourceType": "VECTOR",
                            "vectorImage": {
                                "artifacts": [
                                    {
                                        "width": 200,
                                        "fileIdentifyingUrlPathSegment": "200_200/0/1634030153402?e=1637798400&v=beta&t=If0g__Kio3TesCC0tYnkPgApM0rDg6iF6DWwf7EhhfE",
                                        "expiresAt": 1637798400000,
                                        "height": 125,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 997,
                                        "fileIdentifyingUrlPathSegment": "627_1200/0/1634030153402?e=1637798400&v=beta&t=_8iyLHep7RSKLoV9Gx7Run3oOrqJYgnw_isCcc5-Xvc",
                                        "expiresAt": 1637798400000,
                                        "height": 627,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    },
                                    {
                                        "width": 1280,
                                        "fileIdentifyingUrlPathSegment": "1280/0/1634030153402?e=1637798400&v=beta&t=TurS8e505gfvTtQpbQuu_frTVNQkN9ISnbRSju-8s0A",
                                        "expiresAt": 1637798400000,
                                        "height": 804,
                                        "$type": "com.linkedin.common.VectorArtifact"
                                    }
                                ],
                                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQF7tBJSsZNUoQ/companyUpdate-article-image-shrink_",
                                "$type": "com.linkedin.common.VectorImage"
                            },
                            "displayAspectRatio": 0.625,
                            "$type": "com.linkedin.voyager.common.ImageAttribute"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "subtitle": {
                    "textDirection": "USER_LOCALE",
                    "text": "hubspot.com",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*saveAction": "urn:li:fs_saveAction:(SAVE,urn:li:article:7748953088820494932)",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162379423&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "inlineCta": {
                    "navigationContext": {
                        "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162379423&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                        "trackingActionType": "viewLink",
                        "accessibilityText": "Learn more. View Sponsored Content",
                        "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                    },
                    "text": "Learn more",
                    "$type": "com.linkedin.voyager.feed.render.ButtonComponent"
                },
                "type": "EXTERNAL_FULL",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "Remote and office-based Sales positions!",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ArticleComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853619355792482304",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:share:6853619355536637952",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853619355792482304,urn:li:sponsoredCreative:162379423)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162379423",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE",
                        "authorUrn": "urn:li:sponsoredAccount:506337384",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:506337384",
                        "adUrn": "urn:li:sponsoredCreative:162379423",
                        "activityType": "SPONSORED",
                        "landingUrl": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162379423&hsa_net=linkedin&hsa_ver=3",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162379423,1634756365907)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "2zqdL/8ZTESMrcrhNuRzPQ==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853619355792482304,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "navigationContext": {
                    "actionTarget": "https://www.hubspot.com/careers?utm_source=linkedin&utm_medium=paid&utm_campaign=EB_FR_Q3_2021_Awareness&utm_content=Party_1&hsa_acc=506337384&hsa_cam=603065424&hsa_grp=191018393&hsa_ad=162379423&hsa_net=linkedin&hsa_ver=3&li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                    "trackingActionType": "viewLink",
                    "accessibilityText": "View sponsored page",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "text": "HubSpot is hiring in France! Find out more about our award-winning culture in our careers site and apply to one of our open positions.",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "dashEntityUrn": None,
            "showSocialDetail": None,
            "footer": None,
            "desktopPromoUpdate": None,
            "carouselContent": None,
            "content": {
                "mediaDisplayVariant": "CLASSIC",
                "title": {
                    "textDirection": "FIRST_STRONG",
                    "text": "The Digital Helpdesk - Episode 89: B2B E-Mail-Marketing",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "*videoPlayMetadata": "urn:li:digitalmediaAsset:C4E05AQHlgCRwb-afBw",
                "$type": "com.linkedin.voyager.feed.render.LinkedInVideoComponent"
            },
            "updateMetadata": {
                "urn": "urn:li:activity:6853624041253724160",
                "actionsPosition": "ACTOR_COMPONENT",
                "actionTriggerEnabled": False,
                "detailPageType": "FEED_DETAIL",
                "shareAudience": "PUBLIC",
                "shareUrn": "urn:li:ugcPost:6853623960307847168",
                "excludedFromSeen": True,
                "actions": [
                    {
                        "actionType": "SHARE_VIA",
                        "text": "Copy link to post",
                        "url": "https://www.linkedin.com/feed/update/urn:li:sponsoredContentV2:(urn:li:activity:6853624041253724160,urn:li:sponsoredCreative:162457523)",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    },
                    {
                        "actionType": "REPORT",
                        "targetUrn": "urn:li:sponsoredCreative:162457523",
                        "contentSource": "ADS_TRANSPARENCY_SPONSORED_UPDATE_VIDEO",
                        "authorUrn": "urn:li:sponsoredAccount:500001079",
                        "confirmationText": {
                            "textDirection": "USER_LOCALE",
                            "text": "You'll no longer see this ad in your feed.",
                            "$type": "com.linkedin.voyager.common.TextViewModel"
                        },
                        "text": "Report this ad",
                        "$type": "com.linkedin.voyager.feed.actions.Action"
                    }
                ],
                "trackingData": {
                    "sponsoredTracking": {
                        "tscpUrl": "",
                        "leadTrackingParams": "",
                        "displayFormat": "Promoted",
                        "advertiserUrn": "urn:li:sponsoredAccount:500001079",
                        "adUrn": "urn:li:sponsoredCreative:162457523",
                        "videoBehavior": "OPEN_VIDEO_FULLSCREEN",
                        "activityType": "SPONSORED",
                        "adServingUrn": "urn:li:fsd_adServing:(urn:li:sponsoredCreative:162457523,1634756365902)",
                        "$type": "com.linkedin.voyager.feed.SponsoredMetadata"
                    },
                    "requestId": "no-request-id",
                    "trackingId": "oypp8S9nSRGiFk/BIuuqqA==",
                    "$type": "com.linkedin.voyager.feed.TrackingData"
                },
                "$type": "com.linkedin.voyager.feed.render.UpdateMetadata"
            },
            "detailHeader": None,
            "entityUrn": "urn:li:fs_updateV2:(urn:li:activity:6853624041253724160,FEED_DETAIL,EMPTY,DEFAULT,true)",
            "leadGenFormContent": None,
            "annotation": None,
            "contextualHeader": None,
            "resharedUpdate": None,
            "interstitial": None,
            "contextualDescriptionV2": None,
            "leadGenFormContentV2": None,
            "contextualDescription": None,
            "aggregatedContent": None,
            "$type": "com.linkedin.voyager.feed.render.UpdateV2",
            "socialDetail": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "image": {
                    "accessibilityTextAttributes": [],
                    "attributes": [
                        {
                            "sourceType": "COMPANY_LOGO",
                            "$type": "com.linkedin.voyager.common.ImageAttribute",
                            "*miniCompany": "urn:li:fs_miniCompany:68529"
                        }
                    ],
                    "$type": "com.linkedin.voyager.common.ImageViewModel"
                },
                "name": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 0,
                            "length": 7,
                            "*miniCompany": "urn:li:fs_miniCompany:68529",
                            "type": "COMPANY_NAME",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "HubSpot",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "subDescription": {
                    "textDirection": "USER_LOCALE",
                    "text": "Promoted",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "navigationContext": {
                    "actionTarget": "https://www.linkedin.com/company/hubspot/?miniCompanyUrn=urn%3Ali%3Afs_miniCompany%3A68529",
                    "trackingActionType": "viewCompany",
                    "accessibilityText": "View company: HubSpot",
                    "$type": "com.linkedin.voyager.feed.render.FeedNavigationContext"
                },
                "description": {
                    "textDirection": "USER_LOCALE",
                    "text": "640,226 followers",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "$type": "com.linkedin.voyager.feed.render.ActorComponent"
            },
            "relatedContent": None,
            "header": None,
            "highlightedComments": None,
            "commentary": {
                "templateType": "DEFAULT",
                "translationUrn": "urn:li:fs_translation:(urn:li:ugcPost:6853623960307847168,de)",
                "numLines": 2,
                "text": {
                    "textDirection": "FIRST_STRONG",
                    "attributes": [
                        {
                            "start": 574,
                            "length": 19,
                            "link": "https://www.linkedin.com/feed/hashtag/?keywords=thedigitalhelpdesk&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A6853624041253724160",
                            "type": "HASHTAG",
                            "trackingUrn": "urn:li:hashtag:thedigitalhelpdesk",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        },
                        {
                            "start": 594,
                            "length": 8,
                            "link": "https://www.linkedin.com/feed/hashtag/?keywords=podcast&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A6853624041253724160",
                            "type": "HASHTAG",
                            "trackingUrn": "urn:li:hashtag:podcast",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        },
                        {
                            "start": 603,
                            "length": 15,
                            "link": "https://www.linkedin.com/feed/hashtag/?keywords=emailmarketing&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A6853624041253724160",
                            "type": "HASHTAG",
                            "trackingUrn": "urn:li:hashtag:emailmarketing",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        },
                        {
                            "start": 619,
                            "length": 4,
                            "link": "https://www.linkedin.com/feed/hashtag/?keywords=b2b&highlightedUpdateUrns=urn%3Ali%3Aactivity%3A6853624041253724160",
                            "type": "HASHTAG",
                            "trackingUrn": "urn:li:hashtag:b2b",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        },
                        {
                            "start": 548,
                            "length": 24,
                            "link": "https://lnkd.in/dzmCVsTT",
                            "type": "HYPERLINK",
                            "$type": "com.linkedin.voyager.common.TextAttribute"
                        }
                    ],
                    "text": "E-Mail Marketing ist relativ preiswert und dennoch reichweitenstark.\n\nEs sollte also in keiner B2B-Marketing-Strategie fehlen. Oder?\n\nWorauf es beim B2B E-Mail-Marketing ankommt und welche Rolle Automatisierung und Datenqualität dabei spielen, bespricht Moderator Marvin Hintze in der neuesten Podcast-Episode von The Digital Helpdesk mit Valerie Khalifeh, Marketing Automation Managerin bei morefire und ⚡️Tobias Eickelpasch, Chief E-Mail Officer bei Rock Your E-Mail.\n\nDie gesamte Episode gibt's überall, wo Sie Podcasts hören können - und hier:\nhttps://lnkd.in/dzmCVsTT\n\n#thedigitalhelpdesk #podcast #emailmarketing #b2b",
                    "$type": "com.linkedin.voyager.common.TextViewModel"
                },
                "originalLanguage": "German",
                "$type": "com.linkedin.voyager.feed.render.TextComponent"
            },
            "additionalContents": None
        },
        {
            "subtext": {
                "text": "Manage and plan your social media content with a handy calendar guide and template.",
                "$type": "com.linkedin.pemberly.text.AttributedText"
            },
            "backgroundImage": {
                "artifacts": [
                    {
                        "width": 200,
                        "fileIdentifyingUrlPathSegment": "shrink_200_200/0/1634241825558?e=1637798400&v=beta&t=bSZl750Fu90B01G37wodoHSN-qu2u5huZvH5aLc5Ztg",
                        "expiresAt": 1637798400000,
                        "height": 104,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 1200,
                        "fileIdentifyingUrlPathSegment": "shrink_627_1200/0/1634241825558?e=1637798400&v=beta&t=UtHLNzTwhP9JC3manM43tF-VH0bu32WVWgNKDslN4YI",
                        "expiresAt": 1637798400000,
                        "height": 627,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 1200,
                        "fileIdentifyingUrlPathSegment": "shrink_1280/0/1634241825558?e=1637798400&v=beta&t=SkcRVJr4tHS3ii39oNJw_X8SMVtHuhVI5ePrHk7X604",
                        "expiresAt": 1637798400000,
                        "height": 627,
                        "$type": "com.linkedin.common.VectorArtifact"
                    },
                    {
                        "width": 1200,
                        "fileIdentifyingUrlPathSegment": "offsite-ads_627_1200/0/1634752531531?e=1637798400&v=beta&t=MmMZKVYcwbE6zWlZBif-JhnYpWeRi9csYqm8FCGNYwM",
                        "expiresAt": 1637798400000,
                        "height": 627,
                        "$type": "com.linkedin.common.VectorArtifact"
                    }
                ],
                "rootUrl": "https://media-exp1.licdn.com/dms/image/sync/D4D18AQHrEE1Q5VnbGg/companyUpdate-article-image-",
                "$type": "com.linkedin.common.VectorImage"
            },
            "privacyPolicyOptIn": False,
            "title": "Social Media Content Calendar",
            "customPrivacyPolicy": {
                "text": " We're committed to your privacy. HubSpot uses the information you provide to us to contact you about our relevant content, products, and services. You may unsubscribe from these communications at any time. For more information, check out our Privacy Policy. ",
                "$type": "com.linkedin.pemberly.text.AttributedText"
            },
            "sponsoredCreative": "urn:li:sponsoredCreative:163052703",
            "entityUrn": "urn:li:fs_leadGenForm:(5528143,-1,-1)",
            "questionSections": [
                {
                    "questions": [
                        {
                            "typeDetails": {
                                "prefilledResponse": {
                                    "text": "aisaev@teamdigitaal.nl",
                                    "$type": "com.linkedin.pemberly.text.AttributedText"
                                },
                                "maxResponseLength": 300,
                                "fieldType": "EMAIL_ADDRESS",
                                "minResponseLength": 1,
                                "$type": "com.linkedin.voyager.feed.shared.TextFieldDetails"
                            },
                            "id": 3,
                            "question": "Email address",
                            "required": True,
                            "$type": "com.linkedin.voyager.feed.shared.LeadGenFormQuestion"
                        },
                        {
                            "typeDetails": {
                                "maxResponseLength": 300,
                                "minResponseLength": 1,
                                "$type": "com.linkedin.voyager.feed.shared.TextFieldDetails"
                            },
                            "id": 5,
                            "question": "Website URL",
                            "required": True,
                            "$type": "com.linkedin.voyager.feed.shared.LeadGenFormQuestion"
                        }
                    ],
                    "editable": True,
                    "$type": "com.linkedin.voyager.feed.shared.LeadGenFormSection"
                },
                {
                    "questions": [
                        {
                            "typeDetails": {
                                "prefilledResponse": {
                                    "text": "Aslan",
                                    "$type": "com.linkedin.pemberly.text.AttributedText"
                                },
                                "maxResponseLength": 300,
                                "minResponseLength": 1,
                                "$type": "com.linkedin.voyager.feed.shared.TextFieldDetails"
                            },
                            "id": 1,
                            "question": "First name",
                            "required": True,
                            "$type": "com.linkedin.voyager.feed.shared.LeadGenFormQuestion"
                        },
                        {
                            "typeDetails": {
                                "prefilledResponse": {
                                    "text": "Isaev",
                                    "$type": "com.linkedin.pemberly.text.AttributedText"
                                },
                                "maxResponseLength": 300,
                                "minResponseLength": 1,
                                "$type": "com.linkedin.voyager.feed.shared.TextFieldDetails"
                            },
                            "id": 2,
                            "question": "Last name",
                            "required": True,
                            "$type": "com.linkedin.voyager.feed.shared.LeadGenFormQuestion"
                        },
                        {
                            "typeDetails": {
                                "prefilledResponse": {
                                    "text": "Team Digitaal",
                                    "$type": "com.linkedin.pemberly.text.AttributedText"
                                },
                                "maxResponseLength": 300,
                                "minResponseLength": 1,
                                "$type": "com.linkedin.voyager.feed.shared.TextFieldDetails"
                            },
                            "id": 4,
                            "question": "Company name",
                            "required": True,
                            "$type": "com.linkedin.voyager.feed.shared.LeadGenFormQuestion"
                        }
                    ],
                    "description": "First name, Last name, and Company name",
                    "title": "Profile information to be submitted",
                    "editable": False,
                    "$type": "com.linkedin.voyager.feed.shared.LeadGenFormSection"
                }
            ],
            "advertiserUrn": None,
            "id": 5528143,
            "socialProof": None,
            "ctaText": "LEARN_MORE",
            "privacyPolicy": {
                "attributes": [
                    {
                        "start": 65,
                        "length": 14,
                        "type": {
                            "url": "https://legal.hubspot.com/privacy-policy",
                            "$type": "com.linkedin.pemberly.text.Hyperlink"
                        },
                        "$type": "com.linkedin.pemberly.text.Attribute"
                    }
                ],
                "text": "We'll send this information to HubSpot, subject to the company's privacy policy",
                "$type": "com.linkedin.pemberly.text.AttributedText"
            },
            "landingPage": {
                "url": "https://offers.hubspot.com/thank-you/social-media-content-calendar?li_fat_id=d3fa2863-d582-4c6f-bc00-2cc2cf0b19a2",
                "text": "Visit company website",
                "$type": "com.linkedin.voyager.common.Link"
            },
            "lastSubmittedAt": None,
            "thankYouCTA": "VIEW_NOW",
            "thankYouMessage": {
                "text": "Visit the link below to view the offer.",
                "$type": "com.linkedin.pemberly.text.AttributedText"
            },
            "testLead": True,
            "$type": "com.linkedin.voyager.feed.shared.LeadGenForm",
            "associatedEntity": None,
            "actor": {
                "urn": "urn:li:company:68529",
                "*followingInfo": "urn:li:fs_followingInfo:urn:li:company:68529",
                "showFollowAction": True,
                "*miniCompany": "urn:li:fs_miniCompany:68529",
                "$type": "com.linkedin.voyager.feed.CompanyActor"
            },
            "submitted": None,
            "leadType": None,
            "consentCheckboxes": []
        }
    ]
}