from urllib.parse import quote_plus

from pymongo import MongoClient
import const


class MongoDBStorage:
    @staticmethod
    def connect_to_mongo():
        password_db = quote_plus(const.PASSWORD)
        # client = MongoClient('mongodb://localhost', connect=True)['Git_education'] # local
        client = MongoClient('mongodb://{login}:{password}@{ip}/{db_name}'.format(
            login=const.LOGIN,
            password=password_db,
            ip=const.IP,
            db_name='admin',
        ))[const.DB_NAME]

        return client

    def __init__(self):
        self.client = self.connect_to_mongo()
        self.git_collection = self.client[const.COLLECTION_NAME]

    def upsert_company_data(self, company_name: str, data: dict):
        self.git_collection.update_one({'_id': company_name}, {"$push": {'results': data}}, upsert=True)

    def get_data_by_company(self, company_name):
        company_data = self.git_collection.find_one({'_id': company_name})
        if not company_data:
            return None
        # company_data['result'] = list(filter(lambda data: data['error'] == False, company_data['results']))[-1]
        company_data['result'] = company_data['results'][-1]
        return company_data

    def clear_results(self, company_list=()):
        params = [{'$set': {'results': {'$slice': ['$results', -1]}}}]
        filter_ = {'_id': {'$in': list(company_list)}} if company_list else {}
        self.git_collection.update_many(filter_, params)
