from pymongo import MongoClient, InsertOne
import requests
from entities import DistributionData, CompanyData, CompanyAddress, AffiliatedPage, CompanySize
import const
from mongodb_storage import MongoDBStorage

mongo_instance = MongoDBStorage()
old_collection = mongo_instance.client[const.COLLECTION_NAME]
new_collection = mongo_instance.client['LinkedIn_scraper_new']

valid_counts = [
            'Accounting',
            'Administrative',
            'ArtsAndDesign',
            'BusinessDevelopment',
            'CommunityAndSocialServices',
            'Consulting',
            'Education',
            'Engineering',
            'Entrepreneurship',
            'Finance',
            'HealthcareServices',
            'HumanResources',
            'InformationTechnology',
            'Legal',
            'Marketing',
            'MediaAndCommunication',
            'MilitaryAndProtectiveServices',
            'Operations',
            'ProductManagement',
            'ProgramAndProjectManagement',
            'Purchasing',
            'QualityAssurance',
            'RealEstate',
            'Research',
            'Sales',
            'Support'
        ]
def transform_results(results, _id):
    new_results = []
    for result in results:
        is_data_old = True if result['data'].get('people') else False
        if is_data_old:
            about_data = result['data']['about']
            if not about_data.get('LinkedInURL'):
                raise ZeroDivisionError
            fecets = result['data']['people']['facet_current_company'] or []
            voyager_data = result['data'].get('voyager', {'distribution' + i: None for i in valid_counts})
            affilated_data = result['data'].get('affilated_pages', {'companies': {}}) or {'companies': {}}
            company_address = about_data['companyAddress'] or {}
            company_size = about_data['companyAddress'] or {}


            data = CompanyData(source=_id,
                               name=about_data['Name'],
                               linkedin_url=about_data['LinkedInURL'],
                               company_address=CompanyAddress(
                                   company_name=_id,
                                   country=company_address.get('country'),
                                   geographic_area=company_address.get('geographicArea'),
                                   city=company_address.get('city'),
                                   postal_code=company_address.get('postalCode'),
                                   description=company_address.get('description'),
                                   street_address_opt_out=company_address.get('streetAddressOptOut'),
                                   headquarter=company_address.get('headquarter'),
                                   line1=company_address.get('line1'),
                               ),
                               distributions=voyager_data,
                               affiliated_page=[AffiliatedPage(k, *v.values()) for k, v in affilated_data['companies'].items()],
                               company_size=CompanySize(_id,
                                                        company_size.get('start'),
                                                        company_size.get('end')),
                               source_url=about_data['SourceUrl'],
                               description=about_data['description'],
                               follower_count =about_data['followerCount'],
                               founded=about_data['founded'],
                               headquarters=about_data['headquarters'],
                               industry=about_data.get('industry'),
                               logo=about_data['logo'],
                               specialities=about_data['specialties'],
                               total_employee_count=about_data['totalEmployeeCount'],
                               others_id=about_data['OthersID'],
                               data_type=about_data['type'],
                               location=about_data['location'],
                               facet_current_company=fecets,
                               )
            new_results.append(data)
        else:
            if not result['error'] and result['data']['source'].startswith('http'):
                result['data']['source'] = result['data']['source'].split('/')[-1]
            new_results.append(result)
    return new_results


new_data = []
for n, record in enumerate(old_collection.find({})):
    if n % 100 == 0:
        print(n)
    try:
        record['results'] = transform_results(record['results'],record['_id'])
        new_data.append(InsertOne(record))

    except ZeroDivisionError:
        pass

new_collection.bulk_write(new_data)
