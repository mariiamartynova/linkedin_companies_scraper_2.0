# Run docker containers
docker compose up -d

# Navigate to scraper container
docker exec -it {linkedin_scraper_1 container id} bash

# Run scraper 
python3 scraper.py 

