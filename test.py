import html
import json
import re
# from bs4 import BeautifulSoup
# import pandas as pd
# company_files = {
#                  'holcim':'holcim.html',
#                  'mailup':'mailup.html',
#                  'liquigasdistribuidora':'liquigasdistribuidora.html'}
# code_tags_all = []
# for company_name, company_file_name in company_files.items():
#     with open(company_file_name, encoding='utf-8', newline='') as f:
#         file = f.read()
#         file = html.unescape(file)
#         soup = BeautifulSoup(file, 'html.parser')
#         code_tags = soup.find_all('code')
#         for code_tag in code_tags:
#             if 'com.linkedin.voyager.deco.organization.web.WebFullCompanyMain' in str(code_tag):
#                 print('b')
#             code_tags_all.append({'company':company_name,'code_tag':str(code_tag)})
# df = pd.DataFrame(code_tags_all)
# df.to_excel('code_tags_industry.xlsx',index=False)
#
#
# with open('holsim.html', encoding='utf-8', newline='') as f:
#     file = f.read()
#     file = html.unescape(file)
#     # business_name = 'Holcim'
#     soup = BeautifulSoup(file,'html.parser')
#     code_tags = soup.find_all('code')
#
#     company_id = '134880'
#     business_name = 'Giles Insurance Brokers'
#     company_dict = json.loads(re.findall(fr'({{"data":.+:{company_id}".+name":"{business_name}".+}})', file)[0])
#     industry_mark = None
#     industry = None
#
#     for value in company_dict['included']:
#         if value.get('*companyIndustries') and value.get('name') == business_name:
#             try:
#                 industry_mark = value['*companyIndustries'][0]
#             except Exception:
#                 industry_mark = None
#
#     if industry_mark:
#         for value in company_dict['included']:
#             if value.get('entityUrn') and value.get('entityUrn') == industry_mark:
#                 try:
#                     industry = value['localizedName']
#                 except Exception:
#                     industry = None
#     print(industry)
#
#
# import requests
#
# proxies = {
#     'http': ':@5.189.151.227:24129',
#   'https': ':@5.189.151.227:24129',
# }
# response = requests.get('https://api.ipify.org?format=json', proxies=proxies)
# print(response.text)
def get_company_dict(business_name, company_id):
    with open('test.html', encoding='utf-8', newline='') as f:
        file = f.read()
        file = html.unescape(file)
        page_response = html.unescape(file)
        replaced_business_name = re.escape(business_name)  # self._replace_spec_chars(business_name)
        regex = '\{"data":.*normalized_company:' + company_id + '".*name":"' + replaced_business_name + '.*\}'
        company_list = [json.loads(re.search(regex, page_response).group(0))]
        if len(company_list) == 1:
            company_dict = company_list[0]
        else:
            raise Exception('no such id in the list')
        return company_dict

get_company_dict('IEC - Israel Electric Corporation חברת החשמל לישראל בע"מ', '8226')